package me.nooneboss

import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import me.nooneboss.plugins.*

fun main() {
    embeddedServer(Netty, port = 1234, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    configureRouting()
}
